'use strict';
var assert = require('assert'),
{webdriver, Builder, By, Key, until} = require('selenium-webdriver');
var expect = require('chai').expect;
const args = process.argv[3];

var mod1 = require('./onTest.js');

module.exports = {

NewTwo: function(driver){

  //it('should check for the title', function() {
    //--opens a browser with the legalmatch URL--//
    //--Wait for the websites title to show before going to the next action--//
  //  driver.wait(until.titleIs('Present Your Case - Legal Category'),100000);
  //}),

  //}),
  it('should choose Most Common Issues ', function() {
  //--you can use id in choosing most common issues--//
  //--chooses the Adoption common issues in family category--//
  var issues = driver.findElement(By.id('chosenCat_$leftId_0')).click();
  issues.then(function(webElement){
    console.log('Element exists on Common Issues');
  },function(err){
    if(err.state && err.state === 'no such element') {
    }else{
      console.log('Alert! Element not found on Common Issues!');
    }
  });
}),
 it('should click the next button', function() {
  var next = driver.findElement(By.className('next')).click();
  next.then(function(webElement){
    console.log('Element exists on next button');
  },function(err){
    if(err.state && err.state === 'no such element') {
    }else{
      console.log('Alert! Element not found on next button!');
    }
  });
});
}
}
